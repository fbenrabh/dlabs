package com.example.dlabs.bo;

import com.example.dlabs.dao.DealDao;
import com.example.dlabs.dto.DealDto;
import com.example.dlabs.model.DealDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
@Service
public class DealBo {
    @Autowired
    private DealDao dealDao;



    public List<DealDto> getAll(){
        DealDto newDeal = new DealDto();

        List<DealDo> Dealsdo = dealDao.findAll();

        List<DealDto> deals = new ArrayList<>();

        for(DealDo deal : Dealsdo){
            newDeal = new DealDto();
            newDeal= map(deal);
            deals.add(newDeal);
        }
        return deals;
    }


    
    public DealDto getOne(int i)
    {
        DealDo dealDo = new DealDo();
        dealDo =  dealDao.findById((long)i);

        DealDto newDeal = new DealDto();


        newDeal=map(dealDo);

        return newDeal;
    }

    public void newOne(@RequestBody DealDto dtoDeal) {
        DealDo newDeal = new DealDo();
        newDeal=map(dtoDeal);
        dealDao.save(newDeal);
    }
    public void editOne (int id,@RequestBody DealDto dealDto) {
        DealDo newDeal = new DealDo();
        newDeal = dealDao.findById(id);
        newDeal=map(dealDto);
        dealDao.save(newDeal);
    }



    public void deleteDeal(final int id) {
        dealDao.deleteById((long)id);
    }


    public DealDto map(DealDo dealDo){
        DealDto dealDto = new DealDto();
        dealDto.setId(dealDo.getId());
        dealDto.setTitle(dealDo.getTitle());
        dealDto.setCreator(dealDo.getCreator());
        dealDto.setCreationDate(dealDo.getCreationDate());
        dealDto.setImgUrl(dealDo.getImgUrl());
        dealDto.setTemperature(dealDo.getTemperature());
        dealDto.setDescription(dealDo.getDescription());
        dealDto.setShopLink(dealDo.getShopLink());
        dealDto.setShopName(dealDo.getShopName());
        dealDto.setPriceOld(dealDo.getPriceOld());
        dealDto.setPriceNew(dealDo.getPriceNew());
        dealDto.setPromoCode(dealDo.getPromoCode());
        return dealDto;
    }

    public DealDo map(DealDto dealDto){

        DealDo dealDo = new DealDo();
        dealDo.setId(dealDto.getId());
        dealDo.setTitle(dealDto.getTitle());
        dealDo.setCreator(dealDto.getCreator());
        dealDo.setCreationDate(dealDto.getCreationDate());
        dealDo.setImgUrl(dealDto.getImgUrl());
        dealDo.setTemperature(dealDto.getTemperature());
        dealDo.setDescription(dealDto.getDescription());
        dealDo.setShopLink(dealDto.getShopLink());
        dealDo.setShopName(dealDto.getShopName());
        dealDo.setPriceOld(dealDto.getPriceOld());
        dealDo.setPriceNew(dealDto.getPriceNew());
        dealDo.setPromoCode(dealDto.getPromoCode());
        return dealDo;
    }

}

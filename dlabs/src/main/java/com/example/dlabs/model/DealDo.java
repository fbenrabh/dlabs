package com.example.dlabs.model;
import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="tbl_deal")
public class DealDo {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String shopLink;
    private String shopName;
    private Long priceOld;
    private Long priceNew;
    private String promoCode;
    private Long temperature;
    private String creator;
    @Column(name="date")

    private Date creationDate;
    private String imgUrl;
    private String description;

    public DealDo() {
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShopLink() {
        return shopLink;
    }

    public String getShopName() {
        return shopName;
    }

    public Long getPriceOld() {
        return priceOld;
    }

    public Long getPriceNew() {
        return priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public Long getTemperature() {
        return temperature;
    }

    public String getCreator() {
        return creator;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public void setPriceOld(Long priceOld) {
        this.priceOld = priceOld;
    }

    public void setPriceNew(Long priceNew) {
        this.priceNew = priceNew;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public void setTemperature(Long temperature) {
        this.temperature = temperature;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


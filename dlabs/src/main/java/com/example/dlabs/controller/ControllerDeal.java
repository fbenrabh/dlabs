package com.example.dlabs.controller;


import com.example.dlabs.bo.DealBo;
import com.example.dlabs.dto.DealDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/deals")
@CrossOrigin(value = "http://localhost:3000")
public class ControllerDeal {


    @Autowired
    private DealBo dealBo;


    @GetMapping("")
    public List<DealDto> findAll(){
        return dealBo.getAll();
    }

    @GetMapping("/{id}")
    public DealDto findOne(@PathVariable int id){
        return dealBo.getOne(id);
    }



    @DeleteMapping(value = "/{id}")
    public List<DealDto> deleteDeal(@PathVariable("id") final int id) {
        dealBo.deleteDeal(id);
        return dealBo.getAll();
    }


    @PostMapping("/createDeal")
    public List<DealDto> createDeal(@RequestBody DealDto dealDto) {
        dealBo.newOne(dealDto);
        return dealBo.getAll();
    }

    @PutMapping("/{id}")
    public List<DealDto> createDeal(@PathVariable("id") int id, @RequestBody DealDto dealDto) {
        dealBo.editOne(id,dealDto);
        return dealBo.getAll();
    }


}

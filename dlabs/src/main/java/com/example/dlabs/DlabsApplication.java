package com.example.dlabs;



import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DlabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DlabsApplication.class, args);
	}


}

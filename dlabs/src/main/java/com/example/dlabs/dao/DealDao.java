package com.example.dlabs.dao;

import com.example.dlabs.model.DealDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DealDao  extends JpaRepository<DealDo,Long> {

    DealDo  findById(long id);
    void deleteById(Long aLong);





    DealDo save(DealDo doDeal);
}


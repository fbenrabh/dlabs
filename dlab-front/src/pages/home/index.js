import {useState,useEffect} from 'react'
import axios from 'axios';
import Deal from '../../components/home/Deal.js';
import React from 'react';
import {URL} from '../../API/dlabsAPI'


const fetchDeals =(setDeals)=>{
    axios.get(`${URL}deals`,config)
        .then(res=>setDeals(res.data))
        .catch(err=>console.error(err))
}

const config={
    Headers: { 
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json' 
    }
}
function Home() {
    const [deals,setDeals]=useState([]);

    useEffect(()=>{
        fetchDeals(setDeals);
    },[])

    return (
      <div  style={{width:600,margin:'10px auto'}}>
        {
            deals.map((deal)=><Deal key={deal.id} deal={deal}/>)
        }
      </div>
    );
}
  
  export default Home;
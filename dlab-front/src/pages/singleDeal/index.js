import React from 'react';
import {useState,useEffect} from 'react'
import axios from 'axios';
import {URL} from '../../API/dlabsAPI'
import {useLocation} from 'react-router-dom';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';


const fetchDeal =(id,setDeal)=>{
    axios.get(`${URL}deals/${id}`,config)
        .then(res=>setDeal(res.data))
        .catch(err=>console.error(err))
}



const config={
  Headers: { 
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json' 
  }
}

function SingleDeal() {
  const [deal,setDeal]=useState();
  const id=useLocation()?.state?.id;
  useEffect(()=>{
    id && fetchDeal(id,setDeal);
},)


    return (
 <div  style={{width:600,margin:'10px auto'}}>
<Card  sx={{ maxWidth: 400 ,marginTop:5,padding:1,border:'thick double  ' }}>
<CardMedia
  component="img"
  alt="green iguana"
  maxheight="300"
  maxWidth="300"
  image={deal?.imgUrl}
  style={{border:'0.1px solid '}}
/>
<CardContent>
  <div style={{display:"flex",justifyContent:"space-between"}}>

  <Typography gutterBottom variant="h5" component="div" >
  {deal?.temperature+" °C "}
  </Typography>
  <Typography gutterBottom variant="h7"     style={{ justifyContent: "flex-end"}}>
  { deal?.creationDate.substring(0,10) +" "} 
  </Typography>
  </div>
  <Typography variant="h5">
  {deal?.title}
  </Typography>
  <Typography color="text.secondary">
  {deal?.description}
  </Typography>
  <a href={`${deal?.shopLink}`} target="_blank" >
      <Button    variant="outlined">Voir Site </Button>
      </a>
</CardContent>
<CardActions>
</CardActions>
</Card>
</div>
);
   
  }
  
  export default SingleDeal;
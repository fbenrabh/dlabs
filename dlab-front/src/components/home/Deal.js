import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Link} from 'react-router-dom';
 
export default function Deal({deal}) {
  return (
    <Card  sx={{ maxWidth: 400 ,marginTop:5,padding:1,border:'thick double  ' }}>
      <CardMedia
        component="img"
        alt="green iguana"
        maxheight="300"
        maxWidth="300"
        image={deal?.imgUrl}
        style={{border:'0.1px solid '}}
      />
      <CardContent>
        <div style={{display:"flex",justifyContent:"space-between"}}>
        <Typography gutterBottom variant="h5" component="div" >
        {deal?.temperature+" °C "}
        </Typography>
        <Typography gutterBottom variant="h7"     style={{ justifyContent: "flex-end"}}>
        { deal?.creationDate.substring(0,10) +" "} 
        </Typography>
        </div>
        <Typography variant="body2" color="text.secondary">
        {deal?.title}
        </Typography>
      </CardContent>
      <CardActions>
      
      <Link 
          to={{
            pathname: '/single',
            state: { id: deal?.id}
          }} 
      >
        <Button    variant="outlined"style={{marginRight:5}}>Plus de détails</Button>
      </Link>
      <a href={`${deal?.shopLink}`} target="_blank" >
      <Button    variant="outlined">Voir Site </Button>
      </a>
      </CardActions>
    </Card>
  );
}
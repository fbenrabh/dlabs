import React from "react";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import {Link} from 'react-router-dom';

export default function Header() {

  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static" style={{ background: '#808080' }} >
      <Toolbar>
        <Typography  variant="h6"  sx={{ flexGrow: 1 }}>
          <Link to='/' style={{textDecoration: 'none',color:"inherit" }}> 
          Delabs
          </Link>
        </Typography>
       
        <Button component={Link} to="/single" color="inherit"  variant="outlined">Ajouter Deal</Button>
      
      </Toolbar>
    </AppBar>
  </Box>
  );
}

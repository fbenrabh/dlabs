import {BrowserRouter as Router, Switch,Route } from 'react-router-dom';
import Home from './pages/home';
import Single from './pages/singleDeal'
import Header from './layout/header';
import Footer from './layout/footer';
import React from 'react';

function App() {
  return (
   <Router>
     <Header/>
     <Switch>
        <Route path='/'  exact component={Home}/>
        <Route path ='/single' exact component={Single}/>
     </Switch>
     <Footer/>
   </Router>
  );
}

export default App;
